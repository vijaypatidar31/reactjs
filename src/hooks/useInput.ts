import React, { SyntheticEvent, useState } from 'react'

function useInput(initialValue: any) {
    let [value, setValue] = useState(initialValue);

    const reset = () => {
        setValue(initialValue)
    }

    const bindInput = {
        value,
        onChange: (e: React.ChangeEvent<HTMLInputElement>) => {
            setValue(e.target.value)
        }
    }

    return [value, bindInput, reset]
}

export default useInput;