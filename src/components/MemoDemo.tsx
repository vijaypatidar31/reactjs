import React, { useState, useMemo } from 'react'

const MemoDemo: React.FC<{}> = (props) => {

    let [num, setNum] = useState(0);

    function fact(n: number): number {
        if (n <= 1) return 1;
        else return n * fact(n - 1);
    }

    //useMemo can be use to increase Performance by avoid recomputing result for input which is same as last time already processed
    let f = useMemo(() => {
        console.log('called for finding fact of ' + num);
        return fact(num)
    }, [num])

    console.log('called MemoDemo')
    return (
        <div style={{ border: "1px solid black" }}>
            <div>Number : {num} and is factorial : {f}</div>
            <div>
                <button onClick={()=>setNum(num+1)}>INC</button>
                <button onClick={()=>setNum(num)}>REFRESH</button>
            </div>
        </div>
    )
}

export default MemoDemo
