import UserCard, { UserProps } from './../components/UserCard'
import UserForm from './../components/UserForm'
import User from './../models/User'
import MousePosition from './MousePosition'

import React, { useEffect,useContext } from 'react'
import { UserContext } from '../context/UserContext'

interface UserListProps {
  users: Array<User>,
  handleCardClick(props: User): void,
  handleOnDelete(id: number): void
}
const UserCardList: React.FC<UserListProps> = (props) => {

  let uc = useContext(UserContext)
  console.log(uc)
  
  let { users } = props;
  useEffect(() => {
    console.log("Use Effect user list changes")
  }, [users])

  return (
    <div className="App">
      {
        users.length > 0 ?
          users.map((user) => <UserCard
            key={user.id + ""}
            user={user}
            handleOnDelete={props.handleOnDelete}
            handleCardClick={props.handleCardClick}
          ></UserCard>)
          : <MousePosition />
      }
    </div>
  );
}

export default UserCardList;
