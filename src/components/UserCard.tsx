import React, { Component, useState } from 'react'
import { UserContext } from '../context/UserContext';
import './../css/user_card.css'
import User from './../models/User'

//crating model for defining what are the properties UserCard Component supports
export interface UserProps {
    user: User
    handleCardClick(props: User): void,
    handleOnDelete(id: number): void
}

class UserCard extends React.Component<UserProps>{


    render() {
        let user = this.props.user;
        let { id, name, email } = this.props.user;

        return (
            <div className="card" >
                <img onClick={() => this.props.handleCardClick(user)} src="https://www.w3schools.com/howto/img_avatar.png" alt="Avatar" style={{ width: "100%" }} />
                <div className="container" >
                    <h4 className={id % 3 == 0 ? "red" : "blue"}><b>{name}</b></h4>
                    <p>{email}</p>
                    <button onClick={() => this.props.handleOnDelete(id)} className="btn-delete">DELETE USER</button>
                </div>
            </div>
        )
    }

    componentDidMount() {
        console.log('componentDidMount')
    }

    componentDidUpdate() {
        // called whenever component need to be re render
        console.log('componentDidUpdate')
    }

    componentWillUnmount() {
        // used for releasing and canceling task(http requests) created by this component and are not required after this component get removed
        console.log('componentWillUnmount')
    }

    shouldComponentUpdate(nextProps: any, nextState: any) {
        // we can Customised the update behaviour when data changes
        console.log('shouldComponentUpdate ', nextProps, nextState)
        return true;
    }
}
UserCard.contextType = UserContext;
export default UserCard;