import React, { Component } from 'react'
import './../css/user_detail.css'

interface UserFormProps {
    onUserSubmit: Function
}

const initialUserState = {
    name: "",
    email: ""
};
export default class UserForm extends Component<UserFormProps, { name: string, email: string }> {
    
    constructor(props: UserFormProps) {
        super(props)
        this.state = initialUserState;
    }

    handleNameChange(name: string) {
        console.log(this.state)
        this.setState({ name: name })
    }

    handleEmailChange(email: string) {
        this.setState({ email: email })
    }

    submitForm() {
        let { name, email } = this.state;
        this.props.onUserSubmit(name, email);
        this.setState(initialUserState);
    }
    
    render() {
        let { name, email } = this.state;
        return (
            <div className="user-detail">
                <label htmlFor="name">Enter Name:</label>
                <input
                    type="text"
                    value={name}
                    onChange={(e) => this.handleNameChange(e.target.value)}
                    id="name"
                ></input>
                <label htmlFor="email">Enter email</label>
                <input
                    type="eamil"
                    value={email}
                    onChange={(e) => this.handleEmailChange(e.target.value)}
                    id="email"
                ></input>
                <button disabled={name === "" || email === ""} onClick={() => this.submitForm()}>Add User</button>
            </div>
        )
    }
}
