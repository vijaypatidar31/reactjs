import React, { useState } from 'react'
import { Link } from 'react-router-dom';

function Home(props: any) {
    let [count, setCount] = useState(50);

    function handle() {
        setCount(count - 1)
    };

    setInterval(handle, 1000)
    setTimeout(() => {
        props.history.push('/about')
    }, 50000);

    let posts = [1, 2, 3, 4, 5, 6];
    return (
        <div>
            Home
            <div>
                Redireting to about in {count} seconds
            </div>
            <ul>
                {posts.map((post => <div><Link to={"/posts/"+post}>{"Post "+post }</Link></div>))}
            </ul>
        </div>
    )
}

export default Home
