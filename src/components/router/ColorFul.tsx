import React from 'react'
import PropTypes from 'prop-types'

export default (WrappedComponent:React.FC) => {
    const hocComponent = ({ ...props }) => <WrappedComponent {...props} />

    hocComponent.propTypes = {

    }

    return hocComponent
}
