import React, { useState, useEffect } from 'react'

export default function MousePosition() {
    let [x, setX] = useState(0);
    let [y, setY] = useState(0);

    function recordMouse(e: MouseEvent) {
        setX(e.clientX);
        setY(e.clientY);
    }

    useEffect(() => {
        // setting mouse listner
        console.log('Adding event listener ')
        window.addEventListener('mousemove', recordMouse);

        //returning clean up function that runs after this functional component is removed
        return () => {
            console.log('MousePosition cleaned up ')
            window.removeEventListener('mousemove', recordMouse)
        }

    }, [x]);

    return (
        <div>
            X : {x}, y : {y}
        </div>
    )
}


