import React from 'react'
import useInput from '../hooks/useInput'

function LoginForm() {

    let [name, bindName, resetName] = useInput("");
    let [email, bindEmail, resetEmail] = useInput("");
    let [password, bindPassword, resetPassword] = useInput("");

    function handleSubmit(e: React.FormEvent) {
        e.preventDefault();

        alert(`Name : ${name} Email : ${email} Password : ${password}`)
        resetName();
        resetEmail();
        resetPassword();
    }
    return (
        <div style={
            {
                border: "1px solid red",
                padding: "10px"
            }
        }>
            <form onSubmit={handleSubmit}>
                <input {...bindName} type="text"></input><br />
                <input {...bindEmail} type="text"></input><br />
                <input {...bindPassword} type="password"></input><br />
                <button>Create Account</button>
            </form>
        </div>
    )
}

export default LoginForm
