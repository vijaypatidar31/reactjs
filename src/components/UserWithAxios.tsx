import React, { useEffect, useState } from 'react'
import axios from 'axios'
import User from '../models/User'
import UserCard from './user_card'

export default function UserWithAxios() {

    let [users, setUsers] = useState(Array<User>())

    useEffect(() => {
        axios.get('https://jsonplaceholder.typicode.com/users')
            .then(res => {
                return res.data as Array<User>;
            })
            .then(res => {
                setUsers(res)
            });
        return () => {

        }
    }, [])


    console.log(users)
    return (
        <div>
            {users.length == 0 ? "User not fetched" + users.length : users.map(user => 
                <UserCard user={user} />
            )}
        </div>
    )
}
