import React, { useReducer } from 'react'

const initialState = 0;

const reducer = (state: number, action: string): number => {
    switch (action) {
        case 'increment': return state + 1;
        case 'decrement': return state - 1;
        case 'reset': return initialState;
        default:
            return state;
    }
}

function Counter() {

    let [state, dispatcher] = useReducer(reducer, initialState);

    return (
        <div>

            <h4>Counter : {state}</h4>
            <button onClick={() => dispatcher('increment')}>Inc.</button>
            <button onClick={() => dispatcher('decrement')}>Dec.</button>
            <button onClick={() => dispatcher('reset')}>reset</button>
            <input onChange={(e)=>e.target.value}></input>
        </div>
    )
}

export default Counter
