import React,{Component, useState} from 'react'
import './../css/user_card.css'

export default function UserCard(props) {
    let {name,email,id} = props.user;
    console.log("props")
    return (
        <div className="card" onClick={()=>alert(`user id id ${id}`)}>
            <img src="https://www.w3schools.com/howto/img_avatar.png" alt="Avatar" style={{width:"100%"}} />
            <div className="container">
                <h4><b>{name}</b></h4>
                <p>{email}</p>
            </div>
        </div>
    )
}

