import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Home from './components/router/Home';
import NavBar from './components/router/NavBar';
import Contact from './components/router/Contact';
import About from './components/router/About';
import Post from './components/router/Post';
import PageNotFound from './components/router/PageNotFound';

export default function App() {
  return (
    <Router>
      <div className="App">
        <NavBar />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/contact" component={Contact} />
          <Route path="/about" component={About} />
          <Route path="/posts/:postId" component={Post} />
          <Route component={PageNotFound} />
        </Switch>
      </div>
    </Router>
  );
}

